#include "FLCModuleConnection.h"

FLCModuleConnection::FLCModuleConnection() :
    outputUuid(""),
    inputUuid(""),
    outputWidget(nullptr),
    inputWidget(nullptr),
    outputPortNumber(0),
    inputPortNumber(0)
{

}

FLCModuleConnection::FLCModuleConnection(FLCModuleConnection *fLCModuleConnection) :
    outputUuid(fLCModuleConnection->outputUuid),
    inputUuid(fLCModuleConnection->inputUuid),
    outputWidget(fLCModuleConnection->outputWidget),
    inputWidget(fLCModuleConnection->inputWidget),
    outputPortNumber(fLCModuleConnection->outputPortNumber),
    inputPortNumber(fLCModuleConnection->inputPortNumber)
{

}
