#ifndef FLCMODULECONNECTION_H
#define FLCMODULECONNECTION_H

#include <QObject>
#include <QUuid>

class FLCModuleWidget;

class FLCModuleConnection
{
public:
	FLCModuleConnection();
	FLCModuleConnection( FLCModuleConnection *fLCModuleConnection);

	QUuid outputUuid = "";
	QUuid inputUuid = "";
    FLCModuleWidget *outputWidget = nullptr;
    FLCModuleWidget *inputWidget = nullptr;

	int outputPortNumber;
	int inputPortNumber;
};

#endif // FLCMODULECONNECTION_H
